#!/bin/sh

generate() {
    cat <<EOF
{
    "name": "$name",
    "type": "$type",
    "colors": {
        "activityBarBadge.background": "$bg0",
        "editor.background": "$bg0",
        "input.background": "$bg0",
        "panel.background": "$bg0",
        "statusBar.noFolderBackground": "$bg0",
        "tab.activeBackground": "$bg0",
        "tab.activeBorder": "$bg0",
        "tab.unfocusedActiveBorder": "$bg0",

        "statusBar.debuggingBackground": "$selection",
        "quickInput.background": "$bg1",
        "notifications.background": "$bg1",

        "activityBar.background": "$bg1",
        "editorGroupHeader.noTabsBackground": "$bg1",
        "editorGroupHeader.tabsBackground": "$bg1",
        "tab.hoverBackground": "$selection",
        "sideBar.background": "$bg1",
        "button.background": "$bg2",
        "tab.inactiveBackground": "$bg1",
        "sideBarSectionHeader.background": "$bg2",
        "statusBar.background": "$bg2",
        "editorWidget.background": "$bg1",
        "editorWidget.resizeBorder": "$bg2",
        "editorSuggestWidget.border": "$border2",
        "activityBar.activeBackground": "$bg3_transp",
        "list.focusBackground": "$bg3_transp",
        "list.hoverBackground": "$line_highlight_transp",
        "list.inactiveSelectionBackground": "$line_highlight_transp",

        "editor.selectionHighlightBackground": "$bg4_transp",

        "editorIndentGuide.background": "$guides_inactive",
        "editorRuler.foreground": "$guides_active",
        "editorIndentGuide.activeBackground": "$guides_active",

        "scrollbar.shadow": "$shadow",

        "editor.inactiveSelectionBackground": "$line_highlight_transp",
        "editor.lineHighlightBackground": "$line_highlight_transp",
        "editor.hoverHighlightBackground": "$line_highlight_transp",

        "editor.selectionBackground": "$selection_transp",
        "selection.background": "$selection_transp",

        "list.activeSelectionBackground": "$selection",

        "activityBar.inactiveForeground": "$fg1",
        "activityBar.activeBorder": "$fg2",
        "tab.inactiveForeground": "$fg2",

        "focusBorder": "$focusborder",

        "activityBar.foreground": "$fg0",
        "list.activeSelectionForeground": "$fg0",
        "foreground": "$fg0",
        "activityBarBadge.foreground": "$fg0",
        "badge.foreground": "$fg0",
        "button.foreground": "$fg0",
        "editor.foreground": "$fg0",
        "input.foreground": "$fg0",
        "input.placeholderForeground": "$fg0",
        "sideBar.foreground": "$fg0",
        "statusBar.foreground": "$fg0",
        "tab.activeForeground": "$fg0",

        "contrastBorder": "$contrast_border",
        "focusBorder": "$focus_border",

        "activityBar.activeFocusBorder": "$border2",
        "sideBar.border": "$border2",
        "sideBarSectionHeader.border": "$border2",
        "statusBar.border": "$border2",

        "searchEditor.textInputBorder": "$border",
        "titleBar.border": "$border",
        "editorGroupHeader.border": "$border",
        "activityBar.border": "$border",
        "checkbox.border": "$border",
        "dropdown.border": "$border",
        "editorGroup.border": "$border",
        "editorGroupHeader.tabsBorder": "$border",
        "editorWidget.border": "$border",
        "input.border": "$border",
        "inputOption.activeBorder": "$border",
        "menubar.selectionBorder": "$border",
        "menu.border": "$border",
        "menu.selectionBorder": "$border",
        "notificationCenter.border": "$border",
        "notifications.border": "$border",
        "notificationToast.border": "$border",
        "panel.border": "$border",
        "panelInput.border": "$border",
        "settings.checkboxBorder": "$border",
        "settings.dropdownBorder": "$border",
        "settings.dropdownListBorder": "$border",
        "settings.textInputBorder": "$border",
        "tab.activeBorderTop": "$border",
        "tab.border": "$border",
        "tab.unfocusedActiveBorderTop": "$border",

        "gitDecoration.addedResourceForeground": "$bright_green",
        "gitDecoration.conflictingResourceForeground": "$bright_red",
        "gitDecoration.deletedResourceForeground": "$red",
        "gitDecoration.ignoredResourceForeground": "$fg2",
        "gitDecoration.modifiedResourceForeground": "$dark_yellow",
        "gitDecoration.untrackedResourceForeground": "$green",
        "gitDecoration.submoduleResourceForeground": "$blue",

        "editorGutter.addedBackground": "$green",
        "editorGutter.deletedBackground": "$red",
        "editorGutter.modifiedBackground": "$blue",

        "terminal.background": "$background",
        "terminal.foreground": "$foreground",
        "terminalCursor.background": "$background",
        "terminalCursor.foreground": "$foreground",
        "terminal.ansiBlack": "$black",
        "terminal.ansiRed": "$red",
        "terminal.ansiGreen": "$green",
        "terminal.ansiYellow": "$yellow",
        "terminal.ansiBlue": "$blue",
        "terminal.ansiMagenta": "$magenta",
        "terminal.ansiCyan": "$cyan",
        "terminal.ansiWhite": "$white",
        "terminal.ansiBrightBlack": "$bright_black",
        "terminal.ansiBrightRed": "$bright_red",
        "terminal.ansiBrightGreen": "$bright_green",
        "terminal.ansiBrightYellow": "$bright_yellow",
        "terminal.ansiBrightBlue": "$bright_blue",
        "terminal.ansiBrightMagenta": "$bright_magenta",
        "terminal.ansiBrightCyan": "$bright_cyan",
        "terminal.ansiBrightWhite": "$bright_white"
    },
    "tokenColors": [
        {
            "scope": ["invalid"],
            "settings": { "foreground": "$err", "fontStyle": "bold underline" }
        },
        {
            "scope": ["markup.inserted.diff"],
            "settings": { "foreground": "$diff_add" }
        },
        {
            "scope": ["markup.deleted.diff"],
            "settings": { "foreground": "$diff_rm" }
        },
        {
            "scope": [
                "string",
                "constant",
                "entity.name.import"
            ],
            "settings": { "foreground": "$consts" }
        },
        {
            "scope": [
                "meta.paragraph",
                "string.unquoted.argument",
                "constant.other.option",
                "punctuation.definition.logical-expression",
                "punctuation.definition.dictionary",
                "punctuation.definition.array",
                "punctuation.definition.evaluation",
                "meta"
            ],
            "settings": { "foreground": "$fg0" }
        },
        {
            "scope": [
                "keyword",
                "fenced_code.block.language",
                "punctuation.separator",
                "constant.character.escape",
                "constant.other.placeholder"
            ],
            "settings": { "foreground": "$keywords" }
        },
        {
            "scope": [
                "entity",
                "storage",
                "support",
                "markup.heading",
                "punctuation.definition.heading",
                "entity.name.section"
            ],
            "settings": { "foreground": "$types" }
        },
        {
            "scope": [
                "variable",
                "entity.name",
                "meta.attribute",
                "markup.inline.raw"
            ],
            "settings": { "foreground": "$vars" }
        },
        {
            "scope": [
                "entity.name.function",
                "support.function",
                "meta.function-call",
                "meta.function",
                "entity.name.command"
            ],
            "settings": { "foreground": "$funcs" }
        },
        {
            "scope": [
                "comment",
                "punctuation.definition.comment"
            ],
            "settings": { "foreground": "$comments" }
        },
        { "scope": "markup.underline", "settings": { "fontStyle": "underline" } },
        { "scope": "markup.italic", "settings": { "fontStyle": "italic" } },
        {
            "scope": "keyword.operator",
            "settings": { "fontStyle": "none", "foreground": "$fg0" }
        },
        {
            "scope": [
                $($bold_keywords && printf '"%s",\n' keyword)
                "strong",
                "emphasis",
                "markup.bold",
                "entity.name.type.org",
                "entity.name.function.org",
                "entity.other.attribute-name.org"
            ],
            "settings": { "fontStyle": "bold" }
        }
    ]
}
EOF
}

light() {
    type=light
    foreground='#000'
    background='#fff'
    black='#eee'
    red='#c00'
    green='#163'
    yellow='#c90'
    blue='#04a'
    magenta='#607'
    cyan='#166'
    white='#222'
    bright_black='#ddd'
    bright_red='#f00'
    bright_green='#385'
    bright_yellow='#da0'
    bright_blue='#05c'
    bright_magenta='#80a'
    bright_cyan='#288'
    bright_white='#333'

    dark_yellow='#750'

    bg0=$background
    bg1='#fafafa'
    bg2='#ddd'
    bg3_transp="#0002"
    bg4_transp="#0004"
    shadow="#0004"
    fg0=$foreground
    fg1='#0009'
    fg2='#0009'
    border='#777'
    border2='#777'
    focus_border='#777'
    contrast_border='#777'
    guides_inactive=$bg3_transp
    guides_active=$bg4_transp
    focusborder='#058'
    line_highlight_transp='#0001'
    selection='#ee7'
    selection_transp='#ee7a'
    comments='#055'
    funcs='#058'
    vars='#028'
    types='#408'
    keywords='#805'
    consts='#900'
    err='#e22'
    diff_add=$green
    diff_rm=$red
    bold_keywords=true
}

acme() {
    light
    background='#FFFFEF'
    bg0=$background
    bg1='#EAFFFF'
    bg2='#C9F1F1'
    border='#333'
    border2='#333'
    focus_border='#333'
    contrast_border='#333'
}

dark() {
    type=dark

    foreground='#fff'
    background='#000'
    black='#222'
    red='#c00'
    green='#6ba'
    yellow='#ec0'
    blue='#9cf'
    magenta='#d9f'
    cyan='#4bb'
    white='#ddd'
    bright_black='#333'
    bright_red='#f00'
    bright_green='#8ec'
    bright_yellow='#fe0'
    bright_blue='#bdf'
    bright_magenta='#faf'
    bright_cyan='#5ee'
    bright_white='#f3f3f3'
    dark_yellow='#ec0'

    bg0='#000'
    bg1='#111'
    bg2='#222'
    bg3_transp="#fff2"
    bg4_transp="#fff4"
    shadow="#0004"
    fg0='#fff'
    fg1='#fff9'
    fg2='#fffb'
    guides_inactive=$bg3_transp
    guides_active=$bg4_transp
    border='#383838'
    border2='#383838'
    focus_border='#383838'
    contrast_border='#383838'
    focusborder=$fg2
    line_highlight_transp='#fff2'
    selection='#fff4'
    selection_transp='#fff4'
    comments='#999'
    funcs="#8ec"
    vars="#9bf"
    types="#cbf"
    keywords="#ecf"
    consts="#bdf"

    err='#e22'
    diff_add=$green
    diff_rm=$red
    bold_keywords=false
}

main() {
    mkdir -p themes

    name='Moin Light'
    light
    generate >themes/moin-color-theme.json

    name='Moin Simple Light'
    light
    comments='#055'
    funcs='#000'
    vars='#000'
    types='#000'
    keywords='#000'
    consts='#900'
    generate >themes/moin-simple-color-theme.json

    name='Moin Acme'
    acme
    generate >themes/moin-acme-color-theme.json

    name='Moin Simple Acme'
    acme
    comments='#055'
    funcs='#000'
    vars='#000'
    types='#000'
    keywords='#000'
    consts='#900'
    generate >themes/moin-simple-acme-color-theme.json

    name='Moin Dark'
    dark
    generate >themes/moin-dark-color-theme.json

    name='Moin Soft Dark'
    dark
    bg0='#161616'
    background='#161616'
    bg1='#1a1a1a'
    bg2='#242424'
    border='#000'
    border2='#000'
    focus_border='#000'
    contrast_border='#000'
    generate >themes/moin-soft-dark-color-theme.json
}

main "$@"
