VERSION := $(shell sed -E -n 's/^\s*"version": "(.*)".*$$/\1/p' < package.json)
PKGNAME := $(shell sed -E -n 's/^\s*"name": "(.*)".*$$/\1/p' < package.json)
CODE := $(shell for cmd in code-oss codium code; do command -v "$$cmd" && break; done)

all:
	./gen.sh
	vsce package

install:
	$(CODE) --install-extension $(PKGNAME)-$(VERSION).vsix
