# Moin theme

A couple of high contrast themes.

Inspired by [modus-themes](https://gitlab.com/protesilaos/modus-themes) (*mo*dus-*in*spired, y'know).

## Screenshots

![light](https://imgur.com/XmUaMhH.png)

![acme simple](https://imgur.com/bGGiLy7.png)

![dark](https://i.imgur.com/fwRzRKw.png)

![soft dark](https://i.imgur.com/IeTFN9Z.png)
